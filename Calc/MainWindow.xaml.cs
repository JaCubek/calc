﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool operationmade;
        public double resultnumber;
        public double operationnumber;
        public char lastsign;

        public MainWindow()
        {
            InitializeComponent();
            screen.Text = "0";
            operationmade = true;
            screentop.Text = null;
        }


        private void NumButton_Click(object sender, RoutedEventArgs e)
        {
            var number = (sender as Button).Content;
            if (screen.Text == "0")
            {
                screen.Text = number.ToString();
            }
            else
            {
                screen.Text += number.ToString();
            }
            

        }

        private void BackspaceButton_Click(object sender, RoutedEventArgs e)
        {
            var last = screen.Text.Length;
            // screen.Text -= screen.Text(last);
            //MessageBox.Show(ostatnia);
            if (last > 0)
                screen.Text = screen.Text.Substring(0, last - 1);
            if (last == 1)
                screen.Text = "0";

        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            screen.Text = "0";
            screentop.Text = null;
           
            
        }

        private void SquareButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var screennumber = Math.Sqrt(Convert.ToDouble(screen.Text));
                screen.Text = screennumber.ToString();
            }
            catch
            {
            }
        }

        private void MultiButton_Click(object sender, RoutedEventArgs e)
        {
            var sign = Convert.ToChar((sender as Button).Content);

            if (screentop.Text == "")
            {
                resultnumber = Convert.ToDouble(screen.Text);

            }

            operationnumber = Convert.ToDouble(screen.Text);

                    switch (lastsign)
                    {
                        case '*':
                            resultnumber *= operationnumber;
                            break;

                        case '/':
                            
                            resultnumber /= operationnumber;
                            break;

                        case '+':
                            resultnumber += operationnumber;
                            break;

                        case '-':
                            resultnumber -= operationnumber;
                            break;
                default:
                    break;
                    }

          //  MessageBox.Show(operationnumber.ToString() + " " + resultnumber.ToString() + " " + lastsign + " " + operationmade);
            screentop.Text += screen.Text + sign;
            screen.Text = "0";
            lastsign = sign;

            
       } 


        private void PercentButton_Click(object sender, RoutedEventArgs e)
        {
            var percentnumber = Convert.ToDouble(screen.Text);

           // MessageBox.Show(resultnumber.ToString() + " " + percentnumber.ToString());

            resultnumber *= percentnumber / 100;
            screen.Text = resultnumber.ToString();
            screentop.Text = null;
            lastsign = ' ';
        }

        private void ResultButton_Click(object sender, RoutedEventArgs e)
        {
            screentop.Text = null;
            operationnumber = Convert.ToDouble(screen.Text);

            switch (lastsign)
            {
                case '*':
                    resultnumber *= operationnumber;
                    break;
                case '/':
                    resultnumber /= operationnumber;
                    break;
                case '+':
                    resultnumber += operationnumber;
                    break;
                case '-':
                    resultnumber -= operationnumber;
                    break;
            }
            

            screen.Text = resultnumber.ToString();
            resultnumber = 0;
            operationnumber = 0;
            lastsign = ' ';
           
        }

        private void CommaButton_Click(object sender, RoutedEventArgs e)
        {
            screen.Text += ",";
        }

        private void SignButton_Click(object sender, RoutedEventArgs e)
        {
            var last = screen.Text.Length;

            if (screen.Text[0] != '-' && screen.Text[0] != '+')
            {

                    screen.Text = "-" + screen.Text;
               
            }else
            {
                if (screen.Text[0] == '-')
                {
                    screen.Text = screen.Text.Substring(1, last - 1);
                }
                else
                {
                    screen.Text = "-" + screen.Text.Substring(1, last - 1);
                }

            }
               
               
            

            
        }
    }
}
